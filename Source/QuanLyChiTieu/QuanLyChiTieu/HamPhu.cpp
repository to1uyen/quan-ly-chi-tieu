﻿#include"Resource.h"
#include"stdafx.h"
#include"QuanLyChiTieu.h"
#include"Loai.h"



void checkmouse(HWND hwnd)
{
	TRACKMOUSEEVENT temp;
	temp.cbSize = sizeof(TRACKMOUSEEVENT);
	temp.dwFlags = TME_HOVER | TME_LEAVE;
	temp.dwHoverTime = 1;
	temp.hwndTrack = hwnd;
	TrackMouseEvent(&temp);
}




void veHCN(RECT DIEM)
{
	pen = new Pen(Color(255, 0, 0, 0), 1);
	pen->SetDashStyle(DashStyleDash);
	graphic->DrawRectangle(pen, DIEM.left, DIEM.top, DIEM.right - DIEM.left, DIEM.bottom - DIEM.top);
}


HFONT TaoFont(int height, int dodam)
{
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(height, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, dodam,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	return hFont;
}

LVCOLUMN Add(int TextMax, int cx, int Image, WCHAR* str)
{
	LVCOLUMN LvColumn;
	LvColumn.cchTextMax = TextMax;
	LvColumn.cx = cx;
	LvColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
	LvColumn.fmt = LVCFMT_CENTER;
	LvColumn.pszText = str;
	LvColumn.iSubItem = 0;
	return LvColumn;

}

void InItViewColums(HWND hWndListView)
{
	LVCOLUMN Column;
	Column = Add(20, 100, NULL, L"Ngày tháng");
	ListView_InsertColumn(hWndListView, 0, &Column);
	Column = Add(20, 100, NULL, L"Loại");
	ListView_InsertColumn(hWndListView, 1, &Column);
	Column = Add(20, 200, NULL, L"Nội dung");
	ListView_InsertColumn(hWndListView, 2, &Column);
	Column = Add(20, 100, NULL, L"Tổng tiền");
	ListView_InsertColumn(hWndListView, 3, &Column);
}
void CreateListView(HWND hWndparent)
{
	noidung = CreateWindow(WC_LISTVIEW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | LVS_REPORT | LVS_SINGLESEL, 40, 340, 500, 300, hWndparent, (HMENU)IDI_List,0,0);
	ListView_SetExtendedListViewStyle(noidung, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_TRACKSELECT);
	InItViewColums(noidung);
}
void addItem()
{
	LVITEM lvItem = { 0 };
	lvItem.cchTextMax = 50;
	lvItem.iItem = 0;
	lvItem.lParam = NULL;
	lvItem.pszText = NULL;
	lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
	lvItem.iImage = NULL;
	ListView_InsertItem(noidung, &lvItem);

}
void addItemToList(int cot,WCHAR *text)
{

	ListView_SetItemText(noidung, 0, cot, text);

}

void TaoGroupboxthem(HWND hwndgroup, HWND hwndparent,int x,int y, int width, int height)
{
	hwndgroup = CreateWindowEx(0, L"STATIC",L"Thêm chi tiêu" , BS_GROUPBOX | WS_CHILD | WS_VISIBLE, x, y, width, height, hwndparent, NULL,NULL, NULL);
	SendMessage(hwndgroup, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	hwndgroup = CreateWindowEx(0, L"STATIC", L"THÊM CHI TIÊU", WS_CHILD | WS_VISIBLE | SS_LEFT, x + 210, y - 10, 140, 20, hwndparent, NULL, NULL, NULL);
	SendMessage(hwndgroup, WM_SETFONT, WPARAM(TaoFont(20,550)), TRUE);

	hwndgroup = CreateWindow(L"static", L"Loại chi tiêu", WS_CHILD | WS_VISIBLE, 30, 30, 100, 30, hwndparent, (HMENU)109, 0, 0);
	SendMessage(GetDlgItem(hwndparent, 109), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	hwndcboboxLoai = CreateWindow(L"combobox", NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 40, 50, 300, 200, hwndparent, 0, 0, 0);
	SendMessage(hwndcboboxLoai, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	SendMessage(hwndcboboxLoai, CB_ADDSTRING, 210, (LPARAM)L"Ăn uống");
	SendMessage(hwndcboboxLoai, CB_ADDSTRING, 211, (LPARAM)L"Di chuyển");
	SendMessage(hwndcboboxLoai, CB_ADDSTRING, 212, (LPARAM)L"Nhà cửa");
	SendMessage(hwndcboboxLoai, CB_ADDSTRING, 213, (LPARAM)L"Xe cộ");
	SendMessage(hwndcboboxLoai, CB_ADDSTRING, 214, (LPARAM)L"Nhu yếu phẩm");
	SendMessage(hwndcboboxLoai, CB_ADDSTRING, 215, (LPARAM)L"Dịch vụ");

	hwndgroup = CreateWindow(L"static", L"Nội dung chi", WS_CHILD | WS_VISIBLE, 30, 90, 100, 30, hwndparent, (HMENU)207, 0, 0);
	SendMessage(GetDlgItem(hwndparent, 207), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);


	editnoidung = CreateWindowEx(WS_EX_CLIENTEDGE, L"edit", L"", WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL, 40, 110, 500, 30, hwndparent, 0, 0, 0);
	SendMessage(editnoidung, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	hwndgroup = CreateWindow(L"static", L"Số tiền chi", WS_CHILD | WS_VISIBLE, 30, 155, 100, 30, hwndparent, (HMENU)208, 0, 0);
	SendMessage(GetDlgItem(hwndparent, 208), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	edittien = CreateWindowEx(WS_EX_CLIENTEDGE, L"edit", L"", WS_VISIBLE | WS_CHILD | ES_NUMBER | ES_RIGHT, 40, 175, 100, 30, hwndparent, 0, 0, 0);
	SendMessage(edittien, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);


	hwndgroup = CreateWindow(L"static", L"VND", WS_VISIBLE | WS_CHILD, 150, 180, 30, 30, hwndparent, (HMENU)209, 0, 0);
	SendMessage(GetDlgItem(hwndparent, 209), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	buttonthemct = CreateWindow(L"button", L"THÊM", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 40, 230, 120, 30, hwndparent, (HMENU)300, 0, 0);
	SendMessage(buttonthemct, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
}

void TaoGroupBieudo(HWND hwndgroup, HWND hwndparent, int x, int y, int width, int height)
{
	hwndgroup = CreateWindowEx(0, L"STATIC", L"BIỂU ĐỒ", BS_GROUPBOX | WS_CHILD | WS_VISIBLE, x, y, width, height, hwndparent, NULL, NULL, NULL);
	SendMessage(hwndgroup, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

	hwndgroup = CreateWindowEx(0, L"STATIC", L"BIỂU ĐỒ", WS_CHILD | WS_VISIBLE | SS_LEFT, x + 170, y - 10, 80, 20, hwndparent, NULL, NULL, NULL);
	SendMessage(hwndgroup, WM_SETFONT, WPARAM(TaoFont(20, 550)), TRUE);

	
}


void GhiFile(WCHAR* ngay, WCHAR* loai, WCHAR* noidung, WCHAR* tien, WCHAR* tongthunhap, WCHAR* tiendachi, WCHAR* tientietkiem)
{
	wofstream fout;
	locale loc(locale(), new codecvt_utf8<wchar_t>);
	fout.open("chitieu.txt", ios::app);
	fout.imbue(loc);
	fout << ngay <<endl;
	fout << loai << endl;
	fout << noidung << endl;
	fout << tien<<endl;
	fout << tongthunhap << endl;
	fout << tiendachi << endl;
	fout << tientietkiem << endl;
	fout.close();
}

long DocFIle()
{
	wifstream fin;
	locale loc(locale(), new codecvt_utf8<wchar_t>);
	fin.open("chitieu.txt", ios::in);
	fin.imbue(loc);
	wstring ngay, loai, noidung, tien, tongthunhap, tiendachi, tientietkiem;
	while (true)
	{
		getline(fin, ngay);
		if (fin.eof())
			break;
		getline(fin, loai);
		getline(fin, noidung);
		getline(fin, tien);
		getline(fin, tongthunhap);
		getline(fin, tiendachi);
		getline(fin, tientietkiem);


		dsloai.push_back(CLoai(loai, tien));

		SetWindowText(edittienthu, (WCHAR*)tongthunhap.c_str());
		SetWindowText(edittienchi, (WCHAR*)tiendachi.c_str());
		SetWindowText(edittientietkiem, (WCHAR*)tientietkiem.c_str());

		addItem();
		addItemToList(0, (WCHAR*)(ngay.c_str()));
		addItemToList(1, (WCHAR*)(loai.c_str()));
		addItemToList(2, (WCHAR*)(noidung.c_str()));
		addItemToList(3, (WCHAR*)(tien.c_str()));
	}

	
	fin.close();
	return _wtol(tiendachi.c_str());
}


void Dem(long tienchi, double demtien[6])
{
	for (int i = 0; i < 6; i++)
	{
		demtien[i] = 0;
	}
	long tong = 0;
	for (int i = 0; i < dsloai.size(); i++)
	{
		int nhom = dsloai[i].travenhom();
		demtien[nhom] += dsloai[i].GetTien();
	}
	for (int i = 0; i < 5; i++)
	{
		demtien[i] = ((float)demtien[i] / tienchi) * 360;
		tong += (float)demtien[i];
	}
	demtien[5] = (float)360 - tong;
}






void SetBrush()
{

		mau[0] = new SolidBrush(Color(255, 255, 0, 0));
		mau[1]=new SolidBrush(Color(255, 255, 128, 0));
		mau[2]= new SolidBrush(Color(255, 255, 0, 128));
		mau[3]=new SolidBrush(Color(255, 64, 128, 128));
		mau[4]=new SolidBrush(Color(255, 0, 128, 0));
		mau[5]=new SolidBrush(Color(255, 0, 0, 64));
}

void VePie(long dobatdau, long dokethuc,int i)
{
	graphic->FillPie(mau[i], *elip, dobatdau, dokethuc);

}


void Chuthich(HWND hWndparent)
{
	CreateWindow(L"static", L"Ăn uống", WS_CHILD | WS_VISIBLE, 670, 400, 70, 30, hWndparent, (HMENU)222, NULL, NULL);
	SendMessage(GetDlgItem(hWndparent, 222), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
	
	CreateWindow(L"static", L"Di chuyển", WS_CHILD | WS_VISIBLE, 670, 440, 70, 30, hWndparent, (HMENU)223, NULL, NULL);
	SendMessage(GetDlgItem(hWndparent, 223), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
	
	CreateWindow(L"static", L"Nhà cửa", WS_CHILD | WS_VISIBLE, 670, 480, 70, 30, hWndparent, (HMENU)224, NULL, NULL);
	SendMessage(GetDlgItem(hWndparent, 224), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
	
	CreateWindow(L"static", L"Xe cộ", WS_CHILD | WS_VISIBLE, 670, 520, 70, 30, hWndparent, (HMENU)225, NULL, NULL);
	SendMessage(GetDlgItem(hWndparent, 225), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
	
	CreateWindow(L"static", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE, 670, 560, 100, 30, hWndparent, (HMENU)226, NULL, NULL);
	SendMessage(GetDlgItem(hWndparent, 226), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
	
	CreateWindow(L"static", L"Dịch vụ", WS_CHILD | WS_VISIBLE, 670, 600, 70, 30, hWndparent, (HMENU)227, NULL, NULL);
	SendMessage(GetDlgItem(hWndparent, 227), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
	
}




