#pragma once
#include<iostream>
#include <vector>
#include<fstream>

using namespace std;


class CLoai
{
protected:
	wstring tenloai;
	long sotien;
	int read;
	int green;
	int blue;
public:
	CLoai();
	CLoai(wstring, wstring);
	void SetTien(long);
	long GetTien();
	
	int travenhom();
	~CLoai();
};

extern vector <CLoai> dsloai;
