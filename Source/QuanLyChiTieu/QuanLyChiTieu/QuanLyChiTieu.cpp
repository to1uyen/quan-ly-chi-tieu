﻿// QuanLyChiTieu.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "QuanLyChiTieu.h"


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
INT_PTR CALLBACK ChinhSua(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	WndProc_Left(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	WndProc_Right(HWND, UINT, WPARAM, LPARAM);


INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;
	
	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QUANLYCHITIEU, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QUANLYCHITIEU));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QUANLYCHITIEU));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName = 0;// MAKEINTRESOURCE(IDC_QUANLYCHITIEU);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));


	WNDCLASSEX wcex1;
	wcex1.cbSize = sizeof(WNDCLASSEX);

	wcex1.style = CS_HREDRAW | CS_VREDRAW;
	wcex1.lpfnWndProc = WndProc_Left;
	wcex1.cbClsExtra = 0;
	wcex1.cbWndExtra = 0;
	wcex1.hInstance = hInstance;
	wcex1.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QUANLYCHITIEU));
	wcex1.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex1.hbrBackground = CreateSolidBrush(RGB(255,255,255));
	wcex1.lpszMenuName = MAKEINTRESOURCE(IDC_QUANLYCHITIEU);
	wcex1.lpszClassName = L"profile";
	wcex1.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassEx(&wcex1);



	WNDCLASSEX wcex2;
	wcex2.cbSize = sizeof(WNDCLASSEX);

	wcex2.style = CS_HREDRAW | CS_VREDRAW;
	wcex2.lpfnWndProc = WndProc_Right;
	wcex2.cbClsExtra = 0;
	wcex2.cbWndExtra = 0;
	wcex2.hInstance = hInstance;
	wcex2.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QUANLYCHITIEU));
	wcex2.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex2.hbrBackground = CreateSolidBrush(RGB(153, 217, 234));
	wcex2.lpszMenuName = MAKEINTRESOURCE(IDC_QUANLYCHITIEU);
	wcex2.lpszClassName = L"xuly";
	wcex2.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	RegisterClassEx(&wcex2);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

  /* hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
*/
   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, 0, 0, 1350, 700, NULL, NULL, hInstance, NULL);
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}



//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
HWND hWndHoso;
HWND hWndXuLy;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;
Graphics *graphic;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
		InitCommonControls();
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		hWndHoso = CreateWindow(L"profile", L"", WS_CHILD | WS_VISIBLE, 5, 5, 300, 700, hWnd, 0, 0, 0);
		hWndXuLy = CreateWindow(L"xuly", L"", WS_CHILD | WS_VISIBLE, 310, 5, 1050, 700, hWnd, 0, 0, 0);
		break;

	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
			case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
Image *image;
Rect *rectimage;
HWND edittienchi;
HWND edittienthu;
HWND ngaythang;
HWND edittientietkiem;
HWND buttonsua;

//HWND Dialogbox;


LRESULT CALLBACK WndProc_Left(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	switch (message)
	{
	case WM_CREATE:
		///////////////////
		CreateWindow(L"static", L"Tên:nguyễn Tố Uyên", WS_CHILD | WS_VISIBLE, 120, 10, 200, 30, hWnd, (HMENU)200, 0, 0);
		SendMessage(GetDlgItem(hWnd, 200), WM_SETFONT, (WPARAM)TaoFont(20,600), TRUE);
		//////////////////////////////////////
		CreateWindow(L"static", L"Tuổi : 20 tuổi", WS_CHILD | WS_VISIBLE, 120, 45, 100, 30, hWnd, (HMENU)201, 0, 0);
		SendMessage(GetDlgItem(hWnd, 201), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		///////////////////////////////////
		ngaythang=CreateWindow(DATETIMEPICK_CLASS, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | DTS_RIGHTALIGN, 10, 170, 280, 30, hWnd, (HMENU)202, 0, 0);
		SendMessage(GetDlgItem(hWnd, 202), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		///////////////////////////////
		CreateWindow(L"static", L"Tổng thu nhập:  ", WS_VISIBLE | WS_CHILD, 10, 230, 100, 30, hWnd, (HMENU)203, 0, 0);
		SendMessage(GetDlgItem(hWnd, 203), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		buttonsua = CreateWindow(L"button", L"", WS_VISIBLE | WS_CHILD|BS_ICON, 250, 225, 30, 30,hWnd,(HMENU)250,0,0);
		SendMessage(buttonsua, BM_SETIMAGE, IMAGE_ICON,(LPARAM) LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1)));


		CreateWindow(L"static", L"VNĐ", WS_VISIBLE | WS_CHILD, 210, 230, 40, 30, hWnd, (HMENU)215, 0, 0);
		SendMessage(GetDlgItem(hWnd, 215), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

		edittienthu = CreateWindow(L"static", L"0", WS_VISIBLE | WS_CHILD | ES_NUMBER | ES_READONLY | ES_RIGHT, 100, 230, 100, 30, hWnd, (HMENU)204, 0, 0);
		SendMessage(GetDlgItem(hWnd, 204), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		/////////////////////////////////////
		CreateWindow(L"static", L"Tiền đã chi: ", WS_VISIBLE | WS_CHILD, 10, 260, 100, 30, hWnd, (HMENU)205, 0, 0);
		SendMessage(GetDlgItem(hWnd, 205), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		

		CreateWindow(L"static", L"VNĐ", WS_VISIBLE | WS_CHILD, 210, 260, 40, 30, hWnd, (HMENU)216, 0, 0);
		SendMessage(GetDlgItem(hWnd, 216), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

		edittienchi = CreateWindow(L"static", L"0", WS_CHILD | WS_VISIBLE | ES_NUMBER | ES_RIGHT, 100, 260, 100, 30, hWnd, (HMENU)218, 0, 0);
		SendMessage(GetDlgItem(hWnd, 218), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		////////////////////////////////////
		CreateWindow(L"static", L"Tiền tiết kiệm:  ", WS_VISIBLE | WS_CHILD, 10, 290, 100, 30, hWnd, (HMENU)220, 0, 0);
		SendMessage(GetDlgItem(hWnd, 220), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

		CreateWindow(L"static", L"VNĐ", WS_VISIBLE | WS_CHILD, 210, 290, 40, 30, hWnd, (HMENU)221, 0, 0);
		SendMessage(GetDlgItem(hWnd, 221), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

		edittientietkiem = CreateWindow(L"static", L"0", WS_CHILD | WS_VISIBLE | ES_NUMBER|ES_RIGHT, 100, 290,100, 30, hWnd, (HMENU)219, 0, 0);
		SendMessage(GetDlgItem(hWnd, 219), WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);

		
		////insert avata
		image = new Image(L"IMG_1360.JPG", 0);
		rectimage = new Rect(5, 10, 100, 100);

		break;


	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		bool check;
		// Parse the menu selections:
		switch (wmId)
		{
		case WM_INITDIALOG:
			return 1;
			break;
		
		
		case IDL_BUTTON_SUA:
			long kq;
			kq=DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, ChinhSua);
			if (kq!=0)
			{
				wstring tienthu=L"";
				wsprintf((LPWSTR)tienthu.c_str(), L"%d", kq);
				SetWindowText(edittienthu, (LPWSTR)tienthu.c_str());
			}

			break;
		}
		break;
	case WM_CTLCOLORSTATIC:
		SetTextColor((HDC)wParam, RGB(0, 0, 0));
		SetBkMode((HDC)wParam, TRANSPARENT);
		return (LRESULT)CreateSolidBrush(RGB(255, 255, 255));
	
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		graphic = new Graphics(hdc);
		graphic->DrawImage(image, *rectimage);
		// TODO: Add any drawing code here...
		delete graphic;
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		delete image;
		delete rectimage;
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}
HWND hwndcboboxLoai;
HWND buttonthemct;
HWND editnoidung;
HWND edittien;
HWND noidung;
RECT DIEM;
Pen *pen;

Brush *brush;
Brush* mau[6];
Rect *elip;
HWND gbthem;
HWND gbbieudo;
double sodo[6];
long phantram[6];

Image *image1;
Rect *rectimage1;
Image *image2;
Rect *rectimage2;
Image *image3;
Rect *rectimage3;
Image *image4;
Rect *rectimage4;
Image *image5;
Rect *rectimage5;
Image *image6;
Rect *rectimage6;

long kq1;

LRESULT CALLBACK WndProc_Right(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	HDC hdc;
	PAINTSTRUCT ps;
	WCHAR *t0;
	switch (message)
	{
	case WM_CREATE:
		
		TaoGroupboxthem(gbthem, hWnd, 10, 10, 540, 280);
		TaoGroupBieudo(gbbieudo, hWnd, 570, 10, 430, 630);
		CreateListView(hWnd);
		SendMessage(noidung, WM_SETFONT, (WPARAM)TaoFont(16, 550), TRUE);
		
		 kq1= DocFIle();
		Dem(kq1, sodo);
		
		
	
		elip = new Rect(650, 40, 300, 300);

		SetBrush();
		
		image1 = new Image(L"Anuong.png", 0);
		rectimage1 = new Rect(590, 390, 70, 30);

		image2 = new Image(L"Dichuyen.png", 0);
		rectimage2 = new Rect(590, 430, 70, 30);

		image3 = new Image(L"Nhacua.png", 0);
		rectimage3 = new Rect(590, 470, 70, 30);

		image4 = new Image(L"Xeco.png", 0);
		rectimage4 = new Rect(590, 510, 70, 30);

		image5 = new Image(L"Nhuyeupham.png", 0);
		rectimage5 = new Rect(590, 550, 70, 30);

		image6 = new Image(L"Dichvu.png", 0);
		rectimage6 = new Rect(590, 590, 70, 30);

		Chuthich(hWnd);
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDI_buttonThem:
		{			
					long size0 = GetWindowTextLength(ngaythang);
					long size1 = GetWindowTextLength(hwndcboboxLoai);
					long size2 = GetWindowTextLength(editnoidung);
					long size3 = GetWindowTextLength(edittien);
					long size4 = GetWindowTextLength(edittienchi);
					long size5 = GetWindowTextLength(edittienthu);
					long size6 = GetWindowTextLength(edittientietkiem);

					t0 = new WCHAR[size0 + 1];
					WCHAR *t1 = new WCHAR[size1 + 1];
					WCHAR *t2 = new WCHAR[size2 + 1];
					WCHAR *t3 = new WCHAR[size3 + 1];
					WCHAR *t4 = new WCHAR[size4 + 1];
					WCHAR *t5 = new WCHAR[size5 + 1];
					WCHAR *t6 = new WCHAR[size6 + 1];
					
					GetWindowText(ngaythang, t0, size0 + 1);
					GetWindowTextW(hwndcboboxLoai, t1, size1 + 1);
					GetWindowTextW(editnoidung, t2, size2 + 1);
					GetWindowTextW(edittien, t3, size3 + 1);
					GetWindowText(edittienchi, t4, size4 + 1);
					GetWindowText(edittienthu, t5, size5 + 1);
					GetWindowText(edittientietkiem, t6, size6 + 1);

					long so1 = _wtol(t3);//so tien ben tong tien
					long so2 = _wtol(t4);//tien chi
					long so3 = _wtol(t5);//tien thu
					long so4 = _wtol(t6);//tien tiet kiem

					so2 += so1;//tinh tien chi
					so4 = so3-so2;//tinh tien tiet kiem


					dsloai.push_back(CLoai(t1, t3));


					///tinh tien moi loai
					for (int i = 0; i < 6; i++)
					{
						sodo[i] = 0;
					}
					Dem(so2,sodo);
					
					
					wsprintf(t4, L"%d", so2);
					SetWindowText(edittienchi, t4);

		

					wsprintf(t6, L"%d", so4);
					SetWindowText(edittientietkiem, t6);
					addItem();
					
					addItemToList(0, t0);
					addItemToList(1, t1);
					addItemToList(2, t2);
					addItemToList(3, t3);
					SetWindowText(hwndcboboxLoai, L"Loại");
					SetWindowText(editnoidung,L"");
					SetWindowText(edittien, L"");
					GhiFile(t0, t1, t2, t3,t5,t4,t6);
					InvalidateRect(hWnd, NULL, TRUE);
					break;
		}
			break;

		}
		
		break;
	case WM_CTLCOLORSTATIC:
		SetTextColor((HDC)wParam, RGB(0, 0, 0)); 
		SetBkMode((HDC)wParam, TRANSPARENT);
		return (LRESULT)CreateSolidBrush(RGB(153, 217, 234));
		break;
	case WM_PAINT:

	{
					
					 hdc = BeginPaint(hWnd, &ps);
					 graphic = new Graphics(hdc);
					 graphic->DrawImage(image1, *rectimage1);
					 graphic->DrawImage(image2, *rectimage2);
					 graphic->DrawImage(image3, *rectimage3);
					 graphic->DrawImage(image4, *rectimage4);
					 graphic->DrawImage(image5, *rectimage5);
					 graphic->DrawImage(image6, *rectimage6);

					 int bd = 0;
					 int kt = 0;
					 for (int i = 0; i < 6; i++)
					 {

						 kt = sodo[i];
						 VePie(bd, kt, i);
						 bd = kt + bd;
					 }

					 delete graphic;
					 EndPaint(hWnd, &ps);
					 break;
	}
	case WM_DESTROY:
		delete image1;
		delete rectimage1;
		delete image2;
		delete rectimage2;
		delete image3;
		delete rectimage3;
		delete image4;
		delete rectimage4;
		delete image5;
		delete rectimage5;
		delete image6;
		delete rectimage6;
		break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

INT_PTR CALLBACK ChinhSua(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	wstring tien;
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch ((LOWORD(wParam)))
		{
		case IDOK:
		{
					 WCHAR chinhsua[255];
					 GetDlgItemText(hDlg, IDC_EDIT3, chinhsua, 255);
					 long kq = _wtoi(chinhsua);
					 EndDialog(hDlg, kq);
					 break;
		}
		case IDCANCEL:
		{
						 EndDialog(hDlg, 0);
						 break;
		}
		}
		break;
	}
	return (INT_PTR)FALSE;
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
